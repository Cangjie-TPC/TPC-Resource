<h1 align=center>仓颉三方库资源招募</h1>

 本文收集了一些招募的三方库资源，欢迎各位开发者进行贡献。已有团队/个人认领的招募项目会增加【已由[xxx]()认领】标注。

## 目录

- [目录](#目录)
  - [自定义UI组件](#自定义ui组件)
  - [安全认证](#安全认证)
  - [编码解析](#编码解析)
  - [工具类](#工具类)
  - [图像视频](#图像视频)
  - [网络通信](#网络通信)
  - [其他](#其他)

### 自定义UI组件

- [SmartRefreshLayout](https://github.com/scwang90/SmartRefreshLayout) --  【**已由仓颉库团队认领**】带有动画的上拉刷新和下拉加载的滑动列表库
- [vlayout](https://github.com/alibaba/vlayout) -- 【**已由仓颉库团队认领**】布局扩展组件，提供一整套布局方案和布局间的组件复用功能
- [DanmakuFlameMaster](https://github.com/bilibili/DanmakuFlameMaster/blob/master) -- 【**已由仓颉库团队认领**】弹幕放送、解析与绘制库
- [MultiType](https://github.com/drakeet/MultiType) -- 【**已由仓颉库团队认领**】为ohos的list组件创建多种条目类型的库
- [ohos-PickerView](https://github.com/Bigkoo/Android-PickerView) -- 选择器，包括时间选择、地区选择、分割线设置、文字大小颜色设置
- [RoundedImageView](https://github.com/vinc3m1/RoundedImageView) -- 【**已由仓颉库团队认领**】圆角图片设置组件
- [subsampling-scale-image-view](https://github.com/davemorrissey/subsampling-scale-image-view) -- 【**已由仓颉库团队认领**】视图缩放组件
- [pulltorefresh](https://gitee.com/openharmony-sig/PullToRefresh) -- PullToRefresh是一款OpenHarmony环境下可用的下拉刷新、上拉加载组件。支持设置内置动画的各种属性，支持设置自定义动画，支持lazyForEarch的数据作为数据源。
- [banner](https://github.com/youth5201314/banner) -- 【**已由仓颉库团队认领**】Android广告图片轮播控件，内部基于ViewPager2实现，Indicator和UI都可以自定义。
- [CircleImageView](https://github.com/hdodenhof/CircleImageView) -- 【**已由仓颉库团队认领**】自定义圆形imageview，主要实现圆形图片展示
- [ohos-SwipeLayout](https://github.com/daimajia/AndroidSwipeLayout) --【**已由仓颉库团队认领**】 各种样式的滑动组件
- [TextLayoutBuilder](https://github.com/facebook/TextLayoutBuilder) -- 【**已由仓颉库团队认领**】文本自定义布局组件
- [ImageViewZoom](https://github.com/sephiroth74/ImageViewZoom) -- 【**已由仓颉库团队认领**】图片加载组件，支持缩放和平移
- [recyclerview-animators](https://github.com/wasabeef/recyclerview-animators) -- 【**已由仓颉库团队认领**】带有添加删除动画效果以及整体动画效果的list组件库
- [shimmer-ohos](https://github.com/facebook/shimmer-android) --  【**已由仓颉库团队认领**】供各种形态的页面加载的闪烁效果
- [material-dialogs](https://github.com/afollestad/material-dialogs) -- 自定义弹框组件
- [MaterialDrawer](https://github.com/mikepenz/MaterialDrawer) -- 抽屉组件
- [RecyclerViewPager](https://github.com/lsjwzh/RecyclerViewPager) -- 支持无限循环、左右翻页切换效果、上下翻页切换效果、Material风格的容器
- [Slider](https://github.com/youzan/vant-weapp/tree/dev/dist/slider) -- 自定义滑块组件，支持水平垂直滑动、支持双滑块等特性
- [SwipeItem](https://github.com/youzan/vant-weapp/tree/dev/lib/swipe-cell) -- 【**已由仓颉库团队认领**】自定义侧滑操作菜单
- [WheelPicker](https://github.com/AigeStudio/WheelPicker) -- 【**已由仓颉库团队认领**】滚轮选择器
- [overscroll_decor](https://github.com/EverythingMe/overscroll-décor) -- 【**已由仓颉库团队认领**】UI滚动组件
- [CircleIndicator](https://github.com/ongakuer/CircleIndicator) -- 【**已由仓颉库团队认领**】指示器归一化组件
- [NewbieGuide](https://github.com/huburt-Hu/NewbieGuide) -- 【**已由仓颉库团队认领**】新手引导层的库，通过简洁链式调用，一行代码实现引导层的显示
- [xpopup](https://gitee.com/lxj_gitee/XPopup) -- 内置几种了常用的弹窗，十几种良好的动画，将弹窗和动画的自定义设计的极其简单；目前还没有出现XPopup实现不了的弹窗效果。内置弹窗允许你使用项目已有的布局，同时还能用上XPopup提供的动画，交互和逻辑封装。
- [AnimationEasingFunctions](https://github.com/daimajia/AnimationEasingFunctions) -- 【**已由仓颉库团队认领**】缓动函数是用来描述数值的变化速率，这些数值可以是动画对象的宽高，透明度，旋转，缩放等属性值，它们的变化率可以用函数曲线来表示,制作出更加符合直觉的UI动效,使动画看上去更加真实。
- [android-autofittextview](https://github.com/grantland/android-autofittextview) -- 【**已由仓颉库团队认领**】自动调整文本大小以完全适合其边界的TextView。
- [BottomNavigationBar](https://github.com/Ashok-Varma/BottomNavigation<br/>https://github.com/ibrahimsn98/SmoothBottomBar) -- 底部导航栏归一化组件
- [FloatingMenu](https://github.com/leinardi/FloatingActionButtonSpeedDial) -- 可自定义悬浮菜单样式
- [MaterialProgressBar](https://github.com/zhanghai/MaterialProgressBar) -- 【**已由仓颉库团队认领**】自定义进度条显示效果的归一化组件
- [PagerSlidingTabStrip](https://github.com/jpardogo/PagerSlidingTabStrip) -- 页面和Tab切换联动组件
- [PaletteImageView](https://github.com/DingMouRen/PaletteImageView) -- 一个可以解析图片主体颜色并为图片设置相应颜色阴影的组件
- [Vcode](https://github.com/wux-weapp/wux-weapp/tree/master/src/vcode) -- 【**已由仓颉库团队认领**】图形验证码组件
- [xKeyboard](https://github.com/umicro/uView2.0/tree/master/uni_modules/uview-ui/components/u-keyboard) -- 车牌、数字键盘
- [lrcview](https://github.com/wangchenyan/lrcview) -- 【**已由仓颉库团队认领**】一个音乐播放器自动滚动歌词组件
- [PatternLocker](https://github.com/ihsg/PatternLocker) -- 手势解锁组件
- [ViewSwitcher](https://github.com/leiyun1993/AutoScrollLayout) -- 视图自动切换组件
- [highlightguide]() -- 【**已由仓颉库团队认领**】同NewbieGuide
- [amountinputtext](无) -- 【**已由仓颉库团队认领**】金融类APP在交易时都会用到专有的金额输入框，伴随自定义金额键盘，虽然各家组件的UI样式略有差异，但是，功能上大同小异。OpenHarmony如能提供通用的金额输入框、金额键盘组件，可有效减少重复开发。
- [arouter-api-onActivitResult](https://github.com/hss01248/arouter-api-onActivitResult) -- 用于在各种应用或页面间的跳转和页面间的数据传递，包含跳转拦截和状态回调等功能,使用简单，操作灵活。
- [openharmony-iconfont-cli](https://github.com/iconfont-cli) -- 在openharmony框架中使用iconfont图标
- [ohos_easyUI]() -- 补充了多个OH在即时通讯领域的第三方组件，形成了一套完整的通讯类组件框架，丰富了OH在即时通讯领域的能力。
- [XhsEmoticonsKeyboard](https://github.com/w446108264/XhsEmoticonsKeyboard) -- 开源表情键盘解决方案。
- [Vap](https://github.com/Tencent/vap) -- VAP（VideoAnimationPlayer）是企鹅电竞开发，用于播放酷炫动画的实现方案。
-  [libpag开源库鸿蒙化移植适配](https://github.com/Tencent/libpag/) -- 动画AE源文件缺失，无法导出lottie格式动画，希望支持libpag


### 安全认证

- [appauth](https://github.com/openid/AppAuth-Android) -- 【**已由仓颉库团队认领**】用于与OAuth2.0和OpenIDConnect提供程序进行通信的Android客户端SDK。
- [token](https://github.com/corbel-platform/lib-token<br/>https://github.com/lydell/js-tokens) -- 【**已由仓颉库团队认领**】身份验证令牌库

### 编码解析

- [htmlparser2](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/htmlparser2) -- htmlparser2是一个快速高效的HTML解析器,并用JavaScript语言实现了相关功能
- [icu4j](https://github.com/unicode-org/icu/tree/main/icu4j) -- 【**已由仓颉库团队认领**】Unicode统一组件
- [protobuf_format/xml_js](https://gitee.com/openharmony-tpc/protobuf) -- protobufjs主要功能是序列化和反序列化，更高效，序列化后的体积也很小.
- [protobuf_format/sax](https://gitee.com/openharmony-tpc/protobuf) -- protobufjs主要功能是序列化和反序列化，更高效，序列化后的体积也很小.


### 工具类

- [jbox2d](https://github.com/jbox2d/jbox2d) -- 一个2D的Java物理引擎，C++物理引擎Box2D和LiquidFun的原生Java端口。
- [sanitize-html](https://github.com/apostrophecms/sanitize-html) -- 用于清理HTML内容，防止跨站脚本攻击（XSS）。它可以删除不必要的标签和属性，同时保留你需要的内容。
- [androidvideocache](https://github.com/danikula/AndroidVideoCache) --  【**已由仓颉库团队认领**】视频缓存
- [epublib](https://github.com/psiegman/epublib) -- 读取/写入epub格式文件的核心实现并提供一组epub工具
- [ohos_mail](https://github.com/eclipse-ee4j/mail) -- 检测并解析MIME格式的电子邮件消息流，并构建电子邮件消息的组件合集
- [apacheavro](https://github.com/apache/avro) -- Avro是一个数据序列化的系统，可以将数据结构或对象转化成便于存储或传输的格式，适合于远程或本地大规模数据的存储和交换
- [class-transformer](https://github.com/typestack/class-transformer) -- 对象和类之间基于修饰符的转换、序列化和反序列化
- [msgpack-java](https://github.com/msgpack/msgpack-java) -- 【**已由湖南文理学院[@Yesokim](https://gitcode.com/weixin_64400442)认领**】MessagePack是一种高效的二进制序列化格式。它允许你像JSON一样在多种语言之间交换数据。但它更快更小。小整数被编码成一个字节，典型的短字符串除了字符串本身只需要一个额外的字节。
- [compare-versions](https://www.npmjs.com/package/compare-versions) -- 【**已由[@nutuml](https://gitcode.com/nutuml)认领**】比较版本号
- [androidutilcode](https://github.com/iknow4/Android-utils) -- 安卓工具类库
- [leven](https://github.com/sindresorhus/leven) -- 【**已由东北大学[@Chemxy](https://gitcode.com/Chemxy)认领**】使用Levenshtein距离算法测量两个字符串之间的差异
- [dd-plist](https://github.com/3breadt/dd-plist) -- plist文件解析库
- [Easyrelpace](https://github.com/codsen/codsen<br/>https://github.com/sindresorhus/leven) -- 【已由仓颉库团队认领】字符串对比替换
- [is-png](https://github.com/sindresorhus/is-png) -- 【**已由东北大学[@PermissionDog](https://gitcode.com/permissiondog)认领**】判断是否是png格式文件的库
- [apachejakartacommonsvalida](https://commons.apache.org/proper/commons-validator/<br/>https://github.com/apache/commons-validator) -- Apache Commons Validator为客户端验证和服务器端数据验证提供了构建块。
- [mime](https://www.npmjs.com/package/mime) -- 全面的MIME类型集
- [checksum](https://github.com/dshaw/checksum) -- 【**已由东北大学[@RainBoWli6](https://gitcode.com/RainBoWli6)认领**】计算散列函数的组件，如sha1，MD5等
- [is-webp](https://github.com/sindresorhus/is-webp) -- 【**已由[软通动力]()/东北大学[@ZIYAN137](https://gitcode.com/ZIYAN137)认领**】判断是否是webp的库
- [jmustache](https://github.com/samskivert/jmustache) -- jmustache是一个Java的Mustache模板引擎实现，它是Mustache规范的一个Java实现。Mustache是一个轻量级的模板引擎，它的语法简单易懂，易于学习和使用，被广泛应用于Web应用程序和其它领域。
- [json-schema-validator](https://github.com/box-metadata/json-schema-validator) -- 【**已由仓颉库团队认领**】基于 JSON Schema规范，验证Json数据结构的库。
- [protobuf-format](https://github.com/bivas/protobuf-java-format) -- 【**已由仓颉库团队认领**】基于Google的protobufMessage提供不同格式的序列化和反序列化。允许将默认（字节数组）输出覆盖为基于文本的格式，例如XML、JSON和HTML。
- [soundex-code](https://github.com/words/soundex-code) -- 【**已由仓颉库团队认领**】这个包公开了一个语音算法。这意味着它得到一个特定的字符串（通常是人名），并将其转换成代码，然后可以将其与其他代码（其他名称）进行比较，以检查它们是否（可能）发音相
- [caverphone](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/CaverPhone) -- 【**已由仓颉库团队认领**】CaverPhone算法(语音匹配算法)的JavaScript实现，规则为：将关键字转换为小写，移除不是a-z的字符，按照规则替换指定字符(如字符串起始、结束，文本中包含cq等)，在结尾放置6个1，返回前十个字符，具体参照CaverPhone算法规则。
- [hi-base32](https://github.com/emn178/hi-base32/blob/master/src/base32.js) -- 【**已由东北大学[@Moem](https://gitcode.com/Moem)认领**】一个简单的Base32(RFC4648)编码/解码函数支持UTF-8编码
- [metaphone](https://words.github.io/metaphone/) -- 【**已由兰州交通大学[@yishengTH](https://gitcode.com/yishengTH)认领**】语音算法，支持将一个特定的字符串（通常是一个英文单词），将其转化为一个代码，然后可以将其与其他代码（或其他单词）进行比较，以检查他们是否（可能）发音相同
- [js-md2](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/js-md2Demo) -- 【**已由[@xffish](https://gitcode.com/xffish)认领**】JavaScript的简单MD2哈希函数支持UTF-8编码
- [zxing-text-encoding](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/zxing-text-encodingDemo) -- 应用开发时使用OHPM维护和管理三方库。
- [lz4js](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/lz4jsDemo) -- 【**已由仓颉库团队认领**】lz4js提供lz4格式压缩解压码功能
- [snappyjs](https://github.com/google/snappy) -- 【**已由仓颉库团队认领**】Snappy是一个压缩/解压缩库。它的目标不是最大压缩或与任何其他压缩库的兼容性；相反，它的目标是非常高的速度和合理的压缩。
- [text-encoding](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/textEncodingDemo) -- text-encoding提供文本数据进行编码和解码功能
- [luajava开源库鸿蒙化移植适配](https://github.com/gudzpoz/luajava) -- 提供类似的能力，可以lua和TS互调用
- [Blufi鸿蒙化](https://github.com/EspressifApp/EspBlufiForAndroid?tab=readme-ov-file) -- 用于智能设备的配网和管理，这些智能硬件都是ESP32Combo芯片，Bluefi就是针对该芯片的配方方案
- [ohos-beacon-library](https://github.com/AltBeacon/android-beacon-library) -- 应用与蓝牙信标交互组件
- [FastBle](https://github.com/Jasonchenlijian/FastBle) -- 蓝牙能力集成工具，支持过滤，扫描，链接，读取，写入
- [mbassador](https://gitee.com/openharmony-sig/ohos-mbassador) -- 【**已由仓颉库团队认领**】强大的事件总线，包含事件管理、强/弱引用，事件筛选等
- [adler](https://developer.android.google.cn/reference/java/util/zip/Adler32.html) -- 【**已由仓颉库团队认领**】Adler32算法能力

### 图像视频

- [apache commons imaging](http://commons.apache.org/proper/commons-imaging/) -- 提供了读取和写入多种图像格式的功能。
- [videoCompressor](https://github.com/fishwjy/VideoCompressor) --  【**已由仓颉库团队认领**】使用硬件解码和编码API（MediaCodec）的Android高性能视频压缩器 
- [lena](https://github.com/davidsonfellipe/lena.js) -- 【**已由仓颉库团队认领**】滤镜图像处理库
- [xmlgraphics-batik](https://github.com/apache/xmlgraphics-batik) -- batik项目用于处理可缩放矢量图形（SVG）格式的图像，例如显示、生成、解析或者操作图像。
- [APNG](https://github.com/penfeizhou/APNG4Android) -- 【**已由仓颉库团队认领**】唯品会使用APNG格式动画展示启动页动画、商品动画，OH本身不支持APNG格式的图片编解码，需要三方库实现APNG格式图片加载显示。iOS和Android均使用对应的三方库实现。
- [metadata-extractor](https://github.com/drewnoakes/metadata-extractor) -- 【**已由仓颉库团队认领**】用于从图像、视频和音频文件中提取Exif、IPTC、XMP、ICC和其他元数据的组件
- [video_trimmer](https://github.com/iknow4/Android-Video-Trimmer) -- 【**已由仓颉库团队认领**】实现Android上使用ffmpeg进行视频裁剪，压缩功能。
- [LargeImage](https://github.com/LuckyJayce/LargeImage) -- 【**已由仓颉库团队认领**】提供了在应用程序中管理大尺寸图片的功能。
- [silicompressor](https://github.com/Tourenathan-G5organisation/SiliCompressor) -- 图像/视频压缩库
- [jmespath](https://github.com/jmespath) -- JMESPath是一种JSON查询语言，可以快速解析复杂的Json数据。支持数据提取，数据筛选，数据格式转换。<br/>场景：快速实现数据查找，如某用户名下的某银行卡信息。<br/>

### 网络通信

- [webrtc视频功能移植适配](https://github.com/webrtc) -- 【**已由仓颉库团队认领**】1，适配相机数据获取；<br/>2，适配硬件编解码；
- [WebRTC](https://github.com/webrtc<br/>AppRTC：https://github.com/webrtc/apprtc) -- WebReal-TimeCommunication(WebRTC)是一项允许网页浏览器进行音频、视频以及数据实时通信的技术，它基于网页浏览器，不需要安装额外插件或软件。<br/><br/>该库提供的功能和平台关联性很大，预计需要在鸿蒙适配的WebRtc版本上二次开发<br/>
- [smbj](https://github.com/hierynomus/smbj) -- 主要用于计算机间共享文件，支持安全保护，访问共享目录、打开文件、读写文件等
- [coap](https://gitee.com/openharmony-tpc/ohos_coap) -- libcoap是Coap协议的C语言实现，它是使用Coap协议的工具。ohos_coap是基于libcoapv4.3.1版本，封装napi接口，给上层ts提供coap通信能力的三方库
- [cbor-java](https://github.com/c-rack/cbor-java) -- 【**已由仓颉库团队认领**】java提供cbor（一种提供良好压缩性，扩展性强，不需要进行版本协商的二进制数据交换形式）的工具类
- [apachecommonsnet](https://commons.apache.org/proper/commons-net/) -- Maintenanceandbugfixrelease(Java7).
- [ios-ntp](https://github.com/jbenet/ios-ntp) -- 和服务端校准时间，保证时间三端一致，埋点使用，确保排查问题是时间一致
- [jackrabbit](https://github.com/apache/jackrabbit) -- 文件管理器间接依赖
- [stun-server](https://github.com/enobufs/stun) -- 基于STUN协议的服务开源组件，可获取客户端所在的NAT分配的外部IP地址和端口号
- [eventsource](https://github.com/EventSource/eventsource) -- eventsource三方库是EventSource客户端的纯JavaScript实现。它提供了一种在客户端与服务器之间建立单向持续连接的机制，服务器可以使用这个连接向客户端发送事件更新，而客户端能够实时接收并处理这些更新。
- [FtpServer](NA) -- 支持苹果设备传输文件到单框架手机
- [smack](https://github.com/igniterealtime/Smack) --  【**已由仓颉库团队认领**】开源、高度模块化、易于使用的XMPP客户端库，让开发者可以开发XMPP协议的即时通讯客户端
- [Rocket.Chat.ohos](https://github.com/RocketChat/Rocket.Chat) --  【**已由仓颉库团队认领**】Application interface for server methods and message stream subscriptions.Using this package third party apps can control and query a Rocket.Chat server instance,via REST and Realtime(Websocket) API's.<br/>Designed especially for chat automation, this library makes it easy for application developers to provide the best solutions and experience for their community.via REST and Realtime(Websocket) API's.<br/>Designed especially for chat automation, this library makes it easy for application developers to provide the best solutions and experience for their community.

### 其他

- [PostgreSql鸿蒙化](https://git.postgresql.org/gitweb/?p=postgresql.git;a=summary) -- PostgreSql替换sqlite，优势如下：<br/>1.pg独立进程，不影响服务进程，提高服务进程稳定性<br/>2.pg是多文件存储<br/>3.pg有更好的数据完整性、容错性<br/>4.pg完全开源不受任何商用公司控制。

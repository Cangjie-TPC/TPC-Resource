# Cangjie-TPC 社区开源合规规范

Cangjie-TPC 社区作为 Cangjie 社区开源三方库的主干仓，理应遵循 Cangjie 社区开源合规规范及指导。但由于 Cangjie-TPC 的特殊性，其不涉及该指导文档中的[Cangjie 社区第三方开源软件引入]()部分，但需增加[Cangjie 社区第三方开源软件贡献指导](../Cangjie-Rules/Cangjie社区第三方开源软件贡献指导.md)。
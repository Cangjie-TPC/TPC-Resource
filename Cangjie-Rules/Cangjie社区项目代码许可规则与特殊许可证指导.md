# Cangjie 社区项目代码许可证与特殊许可证指导

## 目的

本指导明确了 Cangjie 社区中的项目代码所采用的许可证，以及接纳的特殊许可证及相关评审流程和规范。

## 范围

本指导仅适用于 Cangjie 社区（包括 Cangjie 和 Cangjie-TPC）中分发的项目代码，不适用于将 Cangjie 社区项目应用于个人或企业以开发其它产品的场景，也不适用单独分发第三方开源软件的场景。

## 定义

Cangjie 项目：Cangjie 用于汇集基于仓颉编程语言的代码示例以及相关教程，仓颉编程语言是一款面向全场景智能的新一代编程语言，主打原生智能化、天生全场景、高性能、强安全。

Cangjie 项目的社区托管地址：https://gitcode.com/Cangjie

Cangjie-TPC 项目：Cangjie-TPC（Third Party Components）用于汇集基于仓颉编程语言开发的开源三方库，帮助开发者方便、快捷、高质量构建仓颉程序。

Cangjie-TPC 项目的社区托管地址：https://gitcode.com/Cangjie-TPC

##  贡献代码采用的许可证白名单

原则上，Cangjie 社区项目贡献代码均应提供源代码，并采用开源促进会 OSI 批准的开源许可证进行分发。

由于开源许可证繁多，Cangjie 项目建议 Cangjie 项目贡献代码优先采用如下许可证白名单中的许可证进行分发。

Cangjie 项目贡献代码许可证白名单包括：

- Apache License 2.0 (Apache-2.0)（适用于用户态代码）
- 3-clause BSD License (BSD-3-Clause)（适用于LiteOS Kernel代码）
- GNU General Public License version 2 (GPL-2.0)（适用于Linux     Kernel代码）

##  Cangjie项目第三方依赖可接纳和不可接纳的许可证名单

Cangjie项目还可能引入或依赖不同的第三方开源软件，这些第三方开源软件采用的许可证的种类和样式可能是多种多样的，为了确保项目的开源属性，要求依赖的第三方开源软件必须具有清晰定义的上游开源社区，且引入第三方开源软件不会引起许可证兼容性法律问题。

### 可接纳的第三方依赖的许可证白名单

与Apache-2.0许可证相兼容的许可证可以被接纳，Cangjie项目建议可优先接纳采用如下许可证的第三方依赖：

- Academic Free License 3.0 (AFL-3.0)

- Apache License 2.0 (Apache-2.0)

- Apache Software License 1.1. Including variants:

  - PHP License 3.01
  - MX4J License

- Boost Software License (BSL-1.0)

- BSD License:

  - 3-clause BSD License
  - 2-clause BSD License
  - DOM4J License
  - PostgreSQL License

- ISC

- ICU

- MIT License (MIT) / X11

- MIT No Attribution License (MIT-0)

- Mulan Permissive Software License v2 (MulanPSL-2.0)

- The Unlicense

- W3C License (W3C)

- University of Illinois/NCSA

- X.Net

- zlib/libpng

- FSF autoconf license

- DejaVu Fonts (Bitstream Vera/Arev licenses)

- OOXML XSD ECMA License

- Microsoft Public License (MsPL)

- Python Software Foundation License

- Python Imaging Library Software License

- Adobe Postcript(R) AFM files

- Boost Software License Version 1.0

- WTF Public License

- The Romantic WTF public license

- UNICODE, INC. LICENSE AGREEMENT - DATA FILES AND SOFTWARE

- Zope Public License 2.0

- ACE license

- Oracle Universal Permissive License (UPL) Version 1.0

- Open Grid Forum License

- Google "Additional IP Rights Grant  (Patents)" file

- Historical Permission Notice and Disclaimer

### 不建议接纳的第三方依赖的许可证名单

原则上，不支持商用的许可证，以及其它包含不合理限制或义务的许可证不建议被接纳，Cangjie项目不建议接纳采用如下许可证的第三方依赖：

- Intel Simplified Software License
- JSR-275 License
- Microsoft Limited Public License
- Amazon Software License (ASL)
- Java SDK for Satori RTM  license
- Redis Source Available License (RSAL)
- Booz Allen Public License
- Confluent Community License Version 1.0
- Any license including the Commons Clause License Condition v1.0
- Creative Commons Non-Commercial variants
- Sun Community Source License 3.0
- GNU GPL 3
- GNU Affero GPL 3
- BSD-4-Clause/BSD-4-Clause (University of California-Specific)    
- Facebook BSD+Patents license
- NPL 1.0/NPL 1.1
- The Solipsistic Eclipse Public License 
- The "Don't Be A Dick" Public License
- JSON License